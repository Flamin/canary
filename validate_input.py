MIN_TEMPERATURE = 0
MAX_TEMPERATURE = 100

MIN_HUMIDITY = 0
MAX_HUMIDITY = 100

TEMPERATURE_TYPE = 'temperature'
HUMIDITY_TYPE = 'humidity'


def get_allowed_types():
    return [TEMPERATURE_TYPE, HUMIDITY_TYPE]

def get_temperature_range():
    return MIN_TEMPERATURE, MAX_TEMPERATURE

def get_humidity_range():
    return MIN_HUMIDITY, MAX_HUMIDITY


def validate_type(vtype):
    if vtype is None:
        return False
    return isTemperature(vtype) or isHumidity(vtype)

def validate_temperature(val):
    if val is None:
        return False
    if not isinstance(val, int):
        return False
    return val >= MIN_TEMPERATURE and val <= MAX_TEMPERATURE

def validate_humidity(val):
    if val is None:
        return False
    if not isinstance(val, int):
        return False
    return val >= MIN_HUMIDITY and val <= MAX_HUMIDITY

def isTemperature(val):
    return val == TEMPERATURE_TYPE

def isHumidity(val):
    return val == HUMIDITY_TYPE