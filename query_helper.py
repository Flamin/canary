def generate_query_clause(vtype, start_dt, end_dt):
    no_type = vtype is None
    no_start = start_dt is None
    no_end = end_dt is None

    if no_type and no_start and no_end:
        return None

    stmt = []
    args = []
    if not no_type:
        stmt.append('type="{}"')
        args.append(vtype)
    if not no_start:
        stmt.append('date_created>{}')
        args.append(int(start_dt))
    if not no_end:
        stmt.append('date_created<{}')
        args.append(int(end_dt))

    query = ""
    for i in range(0, len(stmt)):
        if i > 0:
            query += " and "
        #print("St", type(stmt[i]), stmt[i])
        query += stmt[i]
    return query.format(*args)


def generate_reading_query(device_uuid, vtype, start_dt, end_dt):
    tail = generate_query_clause(vtype, start_dt, end_dt)

    query_base = 'select * from readings where device_uuid="{}"'
    if tail is not None:
        query_base += " and " + tail
    return query_base.format(device_uuid)
        

def generate_min_query(device_uuid, vtype, start_dt, end_dt):
    tail = generate_query_clause(vtype, start_dt, end_dt)

    query_base = 'select * from readings where device_uuid="{}"'
    if tail is not None:
        query_base += " and " + tail
    query_base += " order by value asc"
    return query_base.format(device_uuid)

def generate_max_query(device_uuid, vtype, start_dt, end_dt):
    tail = generate_query_clause(vtype, start_dt, end_dt)

    query_base = 'select * from readings where device_uuid="{}"'
    if tail is not None:
        query_base += " and " + tail
    query_base += " order by value desc"
    return query_base.format(device_uuid)

def generate_median_query(device_uuid, vtype, start_dt, end_dt):
    tail = generate_query_clause(vtype, start_dt, end_dt)

    query_base = 'select * from readings where device_uuid="{}"'
    if tail is not None:
        query_base += " and " + tail
    query_base += ' order by value asc limit 1 offset (select count(*) from readings where device_uuid="{}"'
    if tail is not None:
        query_base += " and " + tail
    query_base += ")/2"
    return query_base.format(device_uuid, device_uuid)


def generate_mean_query(device_uuid, vtype, start_dt, end_dt):
    tail = generate_query_clause(vtype, start_dt, end_dt)

    query_base = 'select avg(value) from readings where device_uuid="{}"'
    if tail is not None:
        query_base += " and " + tail
    return query_base.format(device_uuid)

def generate_mode_query(device_uuid, vtype, start_dt, end_dt):
    tail = generate_query_clause(vtype, start_dt, end_dt)

    query_base = 'select value, count(value) as cnt from readings where device_uuid="{}"'
    if tail is not None:
        query_base += " and " + tail
    query_base += " group by value order by cnt desc"
    return query_base.format(device_uuid)


def generate_quartiles_query(device_uuid, vtype, start_dt, end_dt):
    tail = generate_query_clause(vtype, start_dt, end_dt)

    query_base = 'select value from readings where device_uuid="{}"'
    if tail is not None:
        query_base += " and " + tail
    query_base += " order by value asc"
    return query_base.format(device_uuid)



