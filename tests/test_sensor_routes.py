import json
import pytest
import sqlite3
import time
import unittest

from app import app

class SensorRoutesTestCases(unittest.TestCase):

    def setUp(self):
        # Setup the SQLite DB
        conn = sqlite3.connect('test_database.db')
        conn.execute('DROP TABLE IF EXISTS readings')
        conn.execute('CREATE TABLE IF NOT EXISTS readings (device_uuid TEXT, type TEXT, value INTEGER, date_created INTEGER)')
        
        self.device_uuid = 'test_device'
        self.humidity_device_uuid = 'hum_test_device'

        # Setup some sensor data
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        
        cur.execute('insert into readings (device_uuid,type,value,date_created) VALUES (?,?,?,?)',
                    (self.device_uuid, 'temperature', 22, int(time.time()) - 100))
        cur.execute('insert into readings (device_uuid,type,value,date_created) VALUES (?,?,?,?)',
                    (self.device_uuid, 'temperature', 50, int(time.time()) - 50))
        cur.execute('insert into readings (device_uuid,type,value,date_created) VALUES (?,?,?,?)',
                    (self.device_uuid, 'temperature', 100, int(time.time())))

        cur.execute('insert into readings (device_uuid,type,value,date_created) VALUES (?,?,?,?)',
                    ('other_uuid', 'temperature', 22, int(time.time())))

        for i in range(0, 10):
            cur.execute('insert into readings (device_uuid,type,value,date_created) VALUES (?,?,?,?)',
                        (self.humidity_device_uuid, 'humidity', i * 10, int(time.time()) - 50 * (10 - i)))

        conn.commit()

        app.config['TESTING'] = True

        self.client = app.test_client

    def test_device_readings_get(self):
        # Given a device UUID
        # When we make a request with the given UUID
        request = self.client().get('/devices/{}/readings/'.format(self.device_uuid))

        # Then we should receive a 200
        self.assertEqual(request.status_code, 200)

        # And the response data should have three sensor readings
        self.assertTrue(len(json.loads(request.data)) == 3)


    def test_humidity_device_readings_get(self):
        # Given a device UUID
        # When we make a request with the given UUID
        request = self.client().get('/devices/{}/readings/'.format(self.humidity_device_uuid))

        # Then we should receive a 200
        self.assertEqual(request.status_code, 200)

        # And the response data should have three sensor readings
        self.assertTrue(len(json.loads(request.data)) == 10)


    def test_device_readings_post(self):
        # Given a device UUID
        # When we make a request with the given UUID to create a reading
        request = self.client().post('/devices/{}/readings/'.format(self.device_uuid), data=
            json.dumps({
                'type': 'temperature',
                'value': 100 
            }))

        # Then we should receive a 201
        self.assertEqual(request.status_code, 201)

        # And when we check for readings in the db
        conn = sqlite3.connect('test_database.db')
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute('select * from readings where device_uuid="{}"'.format(self.device_uuid))
        rows = cur.fetchall()

        # We should have three
        self.assertTrue(len(rows) == 4)


    def check_request_code(self, url, code):
        response = self.client().get(url)
        self.assertEqual(response.status_code, code)


    def check_json_request_len(self, url, cnt):
        response = self.client().get(url)
        self.assertEqual(response.status_code, 200)
        json_data = response.get_json()
        self.assertTrue(json_data is not None)
        self.assertTrue(len(json_data) == cnt)


    def check_json_request_field(self, url, field, val):
        response = self.client().get(url)
        self.assertEqual(response.status_code, 200)
        json_data = response.get_json()
        self.assertTrue(json_data is not None)
        self.assertTrue(field in json_data)
        jval = json_data[field]
        self.assertEqual(val, jval)

    def check_json_request_fields(self, url, fields):
        response = self.client().get(url)
        self.assertEqual(response.status_code, 200)
        json_data = response.get_json()
        self.assertTrue(json_data is not None)
        for k,v in fields.items():
            self.assertTrue(k in json_data, str(json_data))
            jval = json_data[k]
            self.assertEqual(v, jval)



    def test_device_readings_get_temperature(self):
        """
        This test should be implemented. The goal is to test that
        we are able to query for a device's temperature data only.
        """
        self.check_request_code('/devices/{}/readings/?type=T'.format(self.device_uuid), 400)
        self.check_json_request_len('/devices/{}/readings/?type=temperature'.format(self.device_uuid), 3)

    
    def test_device_readings_get_humidity(self):
        """
        This test should be implemented. The goal is to test that
        we are able to query for a device's humidity data only.
        """
        self.check_request_code('/devices/{}/readings/?type=Humidity'.format(self.device_uuid), 400)
        self.check_json_request_len('/devices/{}/readings/?type=humidity'.format(self.device_uuid), 0)


    def test_device_readings_get_past_dates(self):
        """
        This test should be implemented. The goal is to test that
        we are able to query for a device's sensor data over
        a specific date range. We should only get the readings
        that were created in this time range.
        """
        tm = int(time.time())
        
        self.check_json_request_len('/devices/{}/readings/?end={}'.format(self.device_uuid, tm + 1), 3)
        self.check_json_request_len('/devices/{}/readings/?start={}'.format(self.device_uuid, tm), 0)
        self.check_json_request_len('/devices/{}/readings/?start={}&end={}'.format(self.device_uuid, tm - 200, tm - 10), 2)



        #print(response.status_code, response.data)

    def test_device_readings_min(self):
        """
        This test should be implemented. The goal is to test that
        we are able to query for a device's min sensor reading.
        """
        tm = int(time.time())

        self.check_json_request_field('/devices/{}/readings/min/?type=temperature'.format(self.device_uuid), "value", 22)
        self.check_json_request_field('/devices/{}/readings/min/?type=temperature&start={}'.format(self.device_uuid, tm - 100), "value", 50)
        self.check_request_code('/devices/{}/readings/min/?type=temperature&start={}'.format(self.device_uuid, tm), 201)
        self.check_json_request_field('/devices/{}/readings/min/?type=temperature&end={}'.format(self.device_uuid, tm), "value", 22)
        self.check_request_code('/devices/{}/readings/min/?type=temperature&end={}'.format(self.device_uuid, tm - 100), 201)
        self.check_json_request_field('/devices/{}/readings/min/?type=temperature&start={}&end={}'.format(self.device_uuid, tm - 50, tm + 50), "value", 100)
        self.check_request_code('/devices/{}/readings/min/?type=humidity&start={}'.format(self.device_uuid, tm), 201)

        self.check_json_request_field('/devices/{}/readings/min/?type=humidity'.format(self.humidity_device_uuid), "value", 0)


    def test_device_readings_max(self):
        """
        This test should be implemented. The goal is to test that
        we are able to query for a device's max sensor reading.
        """
        #self.assertTrue(False)
        tm = int(time.time())

        self.check_json_request_field('/devices/{}/readings/max/?type=temperature'.format(self.device_uuid), "value", 100)
        self.check_json_request_field('/devices/{}/readings/max/?type=temperature&start={}'.format(self.device_uuid, tm - 100), "value", 100)
        self.check_request_code('/devices/{}/readings/max/?type=temperature&start={}'.format(self.device_uuid, tm), 201)
        self.check_json_request_field('/devices/{}/readings/max/?type=temperature&end={}'.format(self.device_uuid, tm), "value", 50)
        self.check_request_code('/devices/{}/readings/max/?type=temperature&end={}'.format(self.device_uuid, tm - 100), 201)
        self.check_json_request_field('/devices/{}/readings/max/?type=temperature&start={}&end={}'.format(self.device_uuid, tm - 50, tm + 50), "value", 100)
        self.check_request_code('/devices/{}/readings/max/?type=humidity&start={}'.format(self.device_uuid, tm), 201)


    def test_device_readings_median(self):
        """
        This test should be implemented. The goal is to test that
        we are able to query for a device's median sensor reading.
        """
        #self.assertTrue(False)
        tm = int(time.time())

        self.check_json_request_field('/devices/{}/readings/median/?type=temperature'.format(self.device_uuid), "value", 50)
        self.check_json_request_field('/devices/{}/readings/median/?type=temperature&start={}'.format(self.device_uuid, tm - 50), "value", 100)
        self.check_json_request_field('/devices/{}/readings/median/?type=temperature&end={}'.format(self.device_uuid, tm), "value", 50)
        self.check_request_code('/devices/{}/readings/median/?type=temperature&end={}'.format(self.device_uuid, tm - 100), 201)
        self.check_json_request_field('/devices/{}/readings/median/?type=humidity'.format(self.humidity_device_uuid), "value", 50)






    def test_device_readings_mean(self):
        """
        This test should be implemented. The goal is to test that
        we are able to query for a device's mean sensor reading value.
        """
        self.check_json_request_field('/devices/{}/readings/mean/?type=temperature'.format(self.device_uuid), "value", (50 + 100 + 22) / 3)
        self.check_json_request_field('/devices/{}/readings/mean/?type=humidity'.format(self.humidity_device_uuid), "value", 45)


    def test_device_readings_mode(self):
        """
        This test should be implemented. The goal is to test that
        we are able to query for a device's mode sensor reading value.
        """

        self.check_json_request_field('/devices/{}/readings/mode/?type=temperature'.format(self.device_uuid), "value", 22)
        self.check_json_request_field('/devices/{}/readings/mode/?type=humidity'.format(self.humidity_device_uuid), "value", 0)


    def test_device_readings_quartiles(self):
        """
        This test should be implemented. The goal is to test that
        we are able to query for a device's 1st and 3rd quartile
        sensor reading value.
        """
        self.check_json_request_fields('/devices/{}/readings/quartiles/?type=temperature'.format(self.device_uuid), {"quartile_1" : 22, "quartile_3" : 100})
        self.check_json_request_fields('/devices/{}/readings/quartiles/?type=humidity'.format(self.humidity_device_uuid), {"quartile_1" : 20, "quartile_3" : 70})


if __name__ == '__main__':
    unittest.main()
    #suite = unittest.TestSuite()
    #suite.addTest(SensorRoutesTestCases("test_device_readings_get_temperature"))
    #suite.addTest(SensorRoutesTestCases("test_device_readings_get_humidity"))
    #suite.addTest(SensorRoutesTestCases("test_device_readings_get_past_dates"))
    #suite.addTest(SensorRoutesTestCases("test_device_readings_min"))
    #suite.addTest(SensorRoutesTestCases("test_device_readings_max"))
    #suite.addTest(SensorRoutesTestCases("test_device_readings_median"))
    #suite.addTest(SensorRoutesTestCases("test_device_readings_mean"))
    #suite.addTest(SensorRoutesTestCases("test_device_readings_mode"))
    #suite.addTest(SensorRoutesTestCases("test_device_readings_quartiles"))


    #runner = unittest.TextTestRunner()
    #runner.run(suite)
