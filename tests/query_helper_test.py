import unittest
import query_helper as Q

class QueryHelperTests(unittest.TestCase):
    def test_query_empty(self):
        query = Q.generate_query_clause(None, None, None)
        self.assertTrue(query is None)

    def test_query_type(self):
        query = Q.generate_query_clause("TestType", None, None)
        self.assertEqual(query, 'type="TestType"')

    def test_query_start(self):
        query = Q.generate_query_clause(None, 100, None)
        self.assertEqual(query, 'date_created>100')

    def test_query_end(self):
        query = Q.generate_query_clause(None, None, 200)
        self.assertEqual(query, 'date_created<200')

    def test_query_start_end(self):
        query = Q.generate_query_clause(None, 100, 200)
        self.assertEqual(query, 'date_created>100 and date_created<200')

    def test_query_type_start_end(self):
        query = Q.generate_query_clause("TestType", 100, 200)
        self.assertEqual(query, 'type="TestType" and date_created>100 and date_created<200')

    def test_query_type_start(self):
        query = Q.generate_query_clause("TestType", 100, None)
        self.assertEqual(query, 'type="TestType" and date_created>100')

    def test_query_type_end(self):
        query = Q.generate_query_clause("TestType", None, 200)
        self.assertEqual(query, 'type="TestType" and date_created<200')

    def test_reading_query_base(self):
        query = Q.generate_reading_query("TestDevice", None, None, None)
        self.assertEqual(query, 'select * from readings where device_uuid="TestDevice"')

    def test_reading_query_params(self):
        query = Q.generate_reading_query("TestDevice", "temperature", 1000, 2000)
        self.assertEqual(query, 'select * from readings where device_uuid="TestDevice" and type="temperature" and date_created>1000 and date_created<2000')

    def test_min_query(self):
        query = Q.generate_min_query("TestDevice", None, None, None)
        self.assertEqual(query, 'select * from readings where device_uuid="TestDevice" order by value asc')

    def test_max_query(self):
        query = Q.generate_max_query("TestDevice", None, None, None)
        self.assertEqual(query, 'select * from readings where device_uuid="TestDevice" order by value desc')

    def test_median_query(self):
        query = Q.generate_median_query("TestDevice", None, None, None)
        self.assertEqual(query, 'select * from readings where device_uuid="TestDevice" order by value asc limit 1 offset (select count(*) from readings where device_uuid="TestDevice")/2')

    def test_mean_query(self):
        query = Q.generate_mean_query("TestDevice", None, None, None)
        self.assertEqual(query, 'select avg(value) from readings where device_uuid="TestDevice"')

    def test_mode_query(self):
        query = Q.generate_mode_query("TestDevice", None, None, None)
        self.assertEqual(query, 'select value, count(value) as cnt from readings where device_uuid="TestDevice" group by value order by cnt desc')


    def test_quartiles_query(self):
        query = Q.generate_quartiles_query("TestDevice", None, None, None)
        self.assertEqual(query, 'select value from readings where device_uuid="TestDevice" order by value asc')



if __name__ == '__main__':
    unittest.main()

