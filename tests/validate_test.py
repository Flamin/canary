import unittest
import validate_input as V
import random
import string


class ValidateTests(unittest.TestCase):
    def test_type_temperature(self):
        self.assertTrue(V.validate_type("temperature"))

    def test_type_humidity(self):
        self.assertTrue(V.validate_type("humidity"))

    def test_type_wrong(self):
        wrong_types = ["Temperature", "Humidity", "temperatur", "temp", "humiditi", "wrong type", "etc"]
        for t in wrong_types:
            self.assertFalse(V.validate_type(t))
        

    def test_temp_range(self):
        t_lower, t_upper = V.get_temperature_range()
        for i in range(0, 10):
            r = random.randint(-1000, 1000)
            rc = V.validate_temperature(r)
            ok = r >= t_lower and r <= t_upper
            self.assertEqual(rc, ok)

    def test_humidity_range(self):
        h_lower, h_upper = V.get_humidity_range()
        for i in range(0, 10):
            r = random.randint(-1000, 1000)
            rc = V.validate_humidity(r)
            ok = r >= h_lower and r <= h_upper
            self.assertEqual(rc, ok)

            



if __name__ == '__main__':
    unittest.main()


    