from flask import Flask, render_template, request, Response
from flask.json import jsonify
import json
import sqlite3
import time
import query_helper
import validate_input


app = Flask(__name__)

# Setup the SQLite DB
conn = sqlite3.connect('database.db')
conn.execute('CREATE TABLE IF NOT EXISTS readings (device_uuid TEXT, type TEXT, value INTEGER, date_created INTEGER)')
conn.close()

        
def get_db_conn():        
    # Set the db that we want and open the connection
    if app.config['TESTING']:
        conn = sqlite3.connect('test_database.db')
    else:
        conn = sqlite3.connect('database.db')
    conn.row_factory = sqlite3.Row
    return conn


@app.route('/devices/<string:device_uuid>/readings/', methods = ['POST', 'GET'])
def request_device_readings(device_uuid):
    """
    This endpoint allows clients to POST or GET data specific sensor types.

    POST Parameters:
    * type -> The type of sensor (temperature or humidity)
    * value -> The integer value of the sensor reading
    * date_created -> The epoch date of the sensor reading.
        If none provided, we set to now.

    Optional Query Parameters:
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    * type -> The type of sensor value a client is looking for
    """
    conn = get_db_conn()
    cur = conn.cursor()
   
    if request.method == 'POST':
        # Grab the post parameters
        post_data = json.loads(request.data)
        sensor_type = post_data.get('type')
        value = post_data.get('value')
        date_created = post_data.get('date_created', int(time.time()))

        if not validate_input.validate_type(sensor_type):
            return "type", 400

        if validate_input.isTemperature(sensor_type):
            if not validate_input.validate_temperature(value):
                return "temperature", 400

        if validate_input.isHumidity(sensor_type):
            if not validate_input.validate_humidity(value):
                return "humidity", 400

        # Insert data into db
        cur.execute('insert into readings (device_uuid,type,value,date_created) VALUES (?,?,?,?)',
                    (device_uuid, sensor_type, value, date_created))
        
        conn.commit()

        # Return success
        return 'success', 201
    else:
        val_type = request.args.get("type")
        start = request.args.get("start")
        end = request.args.get("end")

        if val_type is not None:
            if not validate_input.validate_type(val_type):
                return "type", 400

        # Execute the query
        #cur.execute('select * from readings where device_uuid="{}"'.format(device_uuid))
        query = query_helper.generate_reading_query(device_uuid, val_type, start, end)
        print("Query", query)
        cur.execute(query)
        rows = cur.fetchall()

        # Return the JSON
        return jsonify([dict(zip(['device_uuid', 'type', 'value', 'date_created'], row)) for row in rows]), 200

@app.route('/devices/<string:device_uuid>/readings/min/', methods = ['GET'])
def request_device_readings_min(device_uuid):
    """
    This endpoint allows clients to GET the min sensor reading for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for

    Optional Query Parameters
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """

    val_type = request.args.get("type")
    start = request.args.get("start")
    end = request.args.get("end")

    if val_type is None:
        return "type", 400
    if not validate_input.validate_type(val_type):
        return "type", 400

    query = query_helper.generate_min_query(device_uuid, val_type, start, end)
    print("Query", query)
    conn = get_db_conn()
    cur = conn.cursor()

    cur.execute(query)
    row = cur.fetchone()
    if row is None:
        return "min", 201

    ret = dict(zip(['device_uuid', 'type', 'value', 'date_created'], row))


    # Return the JSON
    return jsonify(ret), 200



@app.route('/devices/<string:device_uuid>/readings/max/', methods = ['GET'])
def request_device_readings_max(device_uuid):
    """
    This endpoint allows clients to GET the max sensor reading for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for

    Optional Query Parameters
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """
    val_type = request.args.get("type")
    start = request.args.get("start")
    end = request.args.get("end")

    if val_type is None:
        return "type", 400
    if not validate_input.validate_type(val_type):
        return "type", 400

    query = query_helper.generate_max_query(device_uuid, val_type, start, end)
    print("Query", query)
    conn = get_db_conn()
    cur = conn.cursor()

    cur.execute(query)
    row = cur.fetchone()
    if row is None:
        return "max", 201

    ret = dict(zip(['device_uuid', 'type', 'value', 'date_created'], row))

    return jsonify(ret), 200
    #return 'Endpoint is not implemented', 501

@app.route('/devices/<string:device_uuid>/readings/median/', methods = ['GET'])
def request_device_readings_median(device_uuid):
    """
    This endpoint allows clients to GET the median sensor reading for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for

    Optional Query Parameters
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """

    val_type = request.args.get("type")
    start = request.args.get("start")
    end = request.args.get("end")

    if val_type is None:
        return "type", 400
    if not validate_input.validate_type(val_type):
        return "type", 400

    query = query_helper.generate_median_query(device_uuid, val_type, start, end)
    print("Query", query)
    conn = get_db_conn()
    cur = conn.cursor()

    cur.execute(query)
    row = cur.fetchone()
    if row is None:
        return "median", 201

    ret = dict(zip(['device_uuid', 'type', 'value', 'date_created'], row))

    return jsonify(ret), 200


@app.route('/devices/<string:device_uuid>/readings/mean/', methods = ['GET'])
def request_device_readings_mean(device_uuid):
    """
    This endpoint allows clients to GET the mean sensor readings for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for

    Optional Query Parameters
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """

    val_type = request.args.get("type")
    start = request.args.get("start")
    end = request.args.get("end")

    if val_type is None:
        return "type", 400
    if not validate_input.validate_type(val_type):
        return "type", 400

    query = query_helper.generate_mean_query(device_uuid, val_type, start, end)
    print("Query", query)
    conn = get_db_conn()
    cur = conn.cursor()

    cur.execute(query)
    row = cur.fetchone()
    if row is None:
        return "mean", 201

    ret = {"value" : row[0]}

    return jsonify(ret), 200


@app.route('/devices/<string:device_uuid>/readings/mode/', methods = ['GET'])
def request_device_readings_mode(device_uuid):
    """
    This endpoint allows clients to GET the mode sensor reading value for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for

    Optional Query Parameters
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """

    val_type = request.args.get("type")
    start = request.args.get("start")
    end = request.args.get("end")

    if val_type is None:
        return "type", 400
    if not validate_input.validate_type(val_type):
        return "type", 400

    query = query_helper.generate_mode_query(device_uuid, val_type, start, end)
    print("Query", query)
    conn = get_db_conn()
    cur = conn.cursor()

    cur.execute(query)
    row = cur.fetchone()
    if row is None:
        return "mode", 201

    ret = {"value" : row[0]}

    return jsonify(ret), 200


@app.route('/devices/<string:device_uuid>/readings/quartiles/', methods = ['GET'])
def request_device_readings_quartiles(device_uuid):
    """
    This endpoint allows clients to GET the 1st and 3rd quartile
    sensor reading value for a device.

    Mandatory Query Parameters:
    * type -> The type of sensor value a client is looking for
    * start -> The epoch start time for a sensor being created
    * end -> The epoch end time for a sensor being created
    """

    val_type = request.args.get("type")
    start = request.args.get("start")
    end = request.args.get("end")

    if val_type is None:
        return "type", 400
    if not validate_input.validate_type(val_type):
        return "type", 400

    query = query_helper.generate_quartiles_query(device_uuid, val_type, start, end)
    print("Query", query)
    conn = get_db_conn()
    cur = conn.cursor()

    cur.execute(query)
    rows = cur.fetchall()
    cnt = len(rows)
    if cnt == 0:
        return "quartiles", 201

    q1 = int(cnt / 4)
    q3 = int(3 * cnt / 4)
    ret = {"quartile_1" : rows[q1][0], "quartile_3" : rows[q3][0]}
    return jsonify(ret), 200


if __name__ == '__main__':
    app.run()
