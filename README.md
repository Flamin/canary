# Canary Platform Homework

## Implementation

### Helper classes

I added a couple of helper classes

- query_helper to generate SQL queries
- validate_input to incapsulate the validation rules

And there are unittests for them: tests/query_helper_test.py, tests/validate_test.py 


### Test coverage

The main functionality has been covered by testcases in tests/test_sensor_routes.py

The test design is not perfect, as I need to implement more testcases than initial structure allowed.

I made multiple checks inside one testcase, like:
```
    def test_device_readings_mean(self):
        self.check_json_request_field('/devices/{}/readings/mean/?type=temperature'.format(self.device_uuid), "value", (50 + 100 + 22) / 3)
        self.check_json_request_field('/devices/{}/readings/mean/?type=humidity'.format(self.humidity_device_uuid), "value", 45)
```

In production code I would have more test classes with each check as testcase, but I decided to keep the suggested test structure.


### Implementation details

- **Add field validation. Only *temperature* and *humidity* sensors are allowed with values between *0* and *100*.**

The validation added into POST part of request_device_readings

- **Add logic for query parameters for *type* and *start/end* dates.**

The logic has been implemented in query_helper.generate_query_clause

- **Implement the min endpoint.**

Implemented in query_helper.generate_min_query as fetchone of sorted cursor by asc

- **Implement the max endpoint.**

Implemented in query_helper.generate_max_query as fetchone of sorted cursor by desc

- **Implement the median endpoint.**

Implemented in query_helper.generate_median_query as offset to halfsize of the cursor

It is a rough calculation because by common description median element consists of 2 values if the set length is even.

Normally I would request more clarification about the expected algorithm and output.

- **Implement the mean endpoint.**

Implemented in query_helper.generate_mean_query with AVG SQL function

- **Implement the mode endpoint.**

Implemented in query_helper.generate_mode_query with SQL GROUP function 

- **Implement the quartile endpoint.**

Did not figure out how to implement that in a single efficient query.

Had a choice to implement with 3 queries or 1 query with other logic in python code.

Decided in favor of python code to avoid the database overloading as it is common bottleneck when a database runs heavy queries.


## Introduction
Imagine a system where hundreds of thousands of Canary like hardware devices are concurrently uploading temperature and humidty sensor data.

The API to facilitate this system accepts creation of sensor records, in addition to retrieval.

These `GET` and `POST` requests can be made at `/devices/<uuid>/readings/`.

Retreival of sensor data should return a list of sensor values such as:

```
    [{
        'date_created': <int>,
        'device_uuid': <uuid>,
        'type': <string>,
        'value': <int>
    }]
```

The API supports optionally querying by sensor type, in addition to a date range.

A client can also access metrics such as the min, max, median, mode and mean over a time range.

These metric requests can be made by a `GET` request to `/devices/<uuid>/readings/<metric>/`

When requesting min, max and median, a single sensor reading dictionary should be returned as seen above.

When requesting the mean or mode, the response should be:

```
    {
        'value': <mean/mode>
    }
```

Finally, the API also supports the retreival of the 1st and 3rd quartile over a specific date range.

This request can be made via a `GET` to `/devices/<uuid>/readings/quartiles/` and should return

```
    {
        'quartile_1': <int>,
        'quartile_3': <int>
    }
```

The API is backed by a SQLite database.

## Getting Started
This service requires Python3. To get started, create a virtual environment using Python3.

Then, install the requirements using `pip install -r requirements.txt`.

Finally, run the API via `python app.py`.

## Testing
Tests can be run via `pytest -v`.

## Tasks
Your task is to fork this repo and complete the following:

- [ ] Add field validation. Only *temperature* and *humidity* sensors are allowed with values between *0* and *100*.
- [ ] Add logic for query parameters for *type* and *start/end* dates.
- [ ] Implement the min endpoint.
- [ ] Implement the max endpoint.
- [ ] Implement the median endpoint.
- [ ] Implement the mean endpoint.
- [ ] Implement the mode endpoint.
- [ ] Implement the quartile endpoint.
- [ ] Add and implement the stubbed out unit tests for your changes.
- [ ] Update the README with any design decisions you made and why.

When you're finished, send your git repo link to Michael Klein at michael@canary.is. If you have any questions, please do not hesitate to reach out!
